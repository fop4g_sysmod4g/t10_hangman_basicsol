#pragma once

#include <string>
#include <vector>
#include "SFML/Graphics.hpp"
#include "GameConsts.h"

/*
* Basic working protoype of hangman, not finished as it only
* supports one word
*/
struct Game
{
	enum class Mode {
		ENTER_NAME,		//get the player's name
		WELCOME,		//game instructions
		GUESS,			//player guesses at the word
		GAME_OVER		//game is over
	};
	Mode mode = Mode::ENTER_NAME; //current mode we are in
	sf::Font font;				//only need one font
	std::string name;			//player name
	char key = GC::NO_KEY;		//current key press 
	int lives = GC::MAX_LIVES;	//when it's zero you are dead
	bool won = false;			//success?
	std::string guess;			//player's current guess
	int tgtWordIdx = 0;			//which wor din the library we are using
	std::vector<char> usedLetters;//track every guess, don't count repeats as that's unfair

	//set things up once at the start
	void Init();
	//update the logic
	void Update(sf::RenderWindow& window);
	//render what we can see
	void Render(sf::RenderWindow& window);
	//check what the player is typing
	void HandleInput(sf::Uint32 key_, sf::RenderWindow& window);

	//player is guessing at our target word
	void UpdateGuess();
	//player's guess character is new and needs checking
	//have they guessed correctly?
	void CheckNewGuess();
	//wait for enter, then choose the word they are trying to guess
	void UpdateWelcome();
	
	//ask the use what their name is
	void RenderEnterName(sf::RenderWindow& window);
	//explaing the game
	void RenderWelcome(sf::RenderWindow& window);
	//display their guesses
	void RenderGuess(sf::RenderWindow& window);
	//it's all over
	void RenderGameOver(sf::RenderWindow& window);
};

