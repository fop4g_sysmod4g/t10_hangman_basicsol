#include <assert.h>

#include "Game.h"

using namespace std;
using namespace sf;

void Game::Init()
{
	srand(0);
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);
}

void Game::HandleInput(Uint32 key_, RenderWindow& window)
{
	key = key_;
	if (key == GC::ESCAPE_KEY)
		window.close();
	if (mode==Mode::ENTER_NAME && (isdigit(key) || isalpha(key)))
		name += static_cast<char>(key);
}

void Game::Update(RenderWindow& window)
{
	switch (mode)
	{
	case Mode::ENTER_NAME:
		if (key == GC::ENTER_KEY && !name.empty())
			mode = Mode::WELCOME;
		break;
	case Mode::WELCOME:
		UpdateWelcome();
		break;
	case Mode::GUESS:
		UpdateGuess();
		break;
	case Mode::GAME_OVER:
		if (key == GC::ENTER_KEY)
			window.close();
		break;
	default:
		break;
	}
}

void Game::UpdateWelcome()
{
	if (key == GC::ENTER_KEY)
	{
		tgtWordIdx = rand() % 5;
		guess = "";
		for (int i = 0; i < (int)GC::WORDS[tgtWordIdx].length(); ++i)
			guess += "_ ";
		mode = Mode::GUESS;
	}
}

void Game::CheckNewGuess()
{
	//see if the character is in the target word
	bool done = true;
	bool found = false;
	for (int i = 0; i < (int)GC::WORDS[tgtWordIdx].length(); ++i)
	{
		char tc = GC::WORDS[tgtWordIdx][i];
		if (tc == key)
		{
			found = true;
			guess[i * 2] = key;
		}

		if (tc != guess[i * 2])
			done = false;
	}

	if (done)
	{
		//all words guessed
		won = true;
		mode = Mode::GAME_OVER;
	}
	if (!found)
	{
		//bad guess
		lives--;
		if (lives == 0)
		{
			won = false;
			mode = Mode::GAME_OVER;
		}
	}
	usedLetters.push_back(key);
}

void Game::UpdateGuess()
{
	if (key != 0)
	{
		//should we ignore this one?
		bool usedBefore = false;
		for (int i = 0; i < (int)usedLetters.size(); ++i)
			if (usedLetters[i] == key)
				usedBefore = true;
		if (!usedBefore)
			CheckNewGuess();
	}
}

void Game::RenderEnterName(RenderWindow& window)
{
	string msg = "Enter your name (press return): ";
	msg += name;
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);
}

void Game::RenderWelcome(RenderWindow& window)
{
	string msg;
	msg = "Welcome ";
	msg += name;
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);
	FloatRect fr = txt2.getLocalBounds();
	txt2.setPosition(txt2.getPosition().x, txt2.getPosition().y + fr.height*2.f);
	txt2.setString("Try and guess each word presented,\nyou can make seven mistakes per word. <press return>");
	window.draw(txt2);
}

void Game::RenderGuess(RenderWindow& window)
{
	string msg;
	msg = "Lives left: ";
	msg += to_string(lives);
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	FloatRect fr = txt2.getLocalBounds();
	window.draw(txt2);

	txt2.setPosition(txt2.getPosition().x, txt2.getPosition().y + fr.height*2.f);
	msg = "Your guess: ";
	msg += guess;
	txt2.setString(msg);
	window.draw(txt2);
}

void Game::RenderGameOver(RenderWindow& window)
{
	string msg;
	msg = "Target word: " + GC::WORDS[tgtWordIdx];
	Text txt(msg, font, 20);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	FloatRect fr = txt.getLocalBounds();
	window.draw(txt);

	if (won)
		msg = "Congratulations winner! :)";
	else
		msg = "Sorry, you lost :(";
	txt.setString(msg);
	txt.setPosition(txt.getPosition().x, txt.getPosition().y + fr.height*2.f);
	window.draw(txt);

	txt.setPosition(txt.getPosition().x, txt.getPosition().y + fr.height*2.f);
	txt.setString("Press return to quit");
	window.draw(txt);
}

void Game::Render(RenderWindow& window)
{
	Text txt("Hangman", font, 30);
	txt.setFillColor(Color::White);
	txt.setOutlineColor(Color::Red);
	txt.setOutlineThickness(5);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.05f);
	window.draw(txt);

	switch (mode)
	{
	case Mode::ENTER_NAME:
		RenderEnterName(window);
		break;
	case Mode::WELCOME:
		RenderWelcome(window);
		break;
	case Mode::GUESS:
		RenderGuess(window);
		break;
	case Mode::GAME_OVER:
		RenderGameOver(window);
		break;
	default:
		assert(false);
	}
	key = 0;
}
